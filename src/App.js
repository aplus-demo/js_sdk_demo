import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div >
      <button onClick={window.initTap}> 初始化SDK </button>
      <button onClick={window.autoLoginTap}> 自动登录 </button>
      <button onClick={window.apjJSLoginTap}> APJ JS 登录 </button>
      <button onClick={window.apjUILoginTap}> APJ 登录页面 </button>
      <button onClick={window.apjUIRegisterTap}> APJ 注册页面 </button>
      <button onClick={window.resetPasswordTap}> APJ 重置密码 </button>
      <button onClick={window.appleUILoginTap}> Apple UI 登录 </button>
      <div id='google_login_btn'> Google UI 登录 </div>
      <button onClick={window.logoutTap}> 退出登录 </button>
    </div>
  );
}

export default App;
